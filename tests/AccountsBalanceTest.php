<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Src\User;

final class AccountsBalanceTest extends TestCase
{
    public function testSetBalance(): void
    {
        $amount = 12;
        $fee = 0.1;

        $user = new User();

        $user->setNewBalance($amount, $fee);

    
        $this->assertEquals(
            $user->current_balance,
            $amount - $fee
        );
    }
}
