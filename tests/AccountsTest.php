<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Src\User;

final class AccountsTest extends TestCase
{
    public function testUserName(): void
    {

        $user = new User();

        $user->setName();

        $this->assertEquals(
            'John Doe',
            $user->name
        );
    }
}
