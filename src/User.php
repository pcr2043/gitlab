<?php

namespace Src;

class User
{

    public $first_name;
    public $last_name;
    public $current_balance = 10;
    public $name;

    
    public function setNewBalance($amount, $fee){

        $this->current_balance = $amount - $fee;
    }

    public function setFirstName($value)
    {
        return $this->first_name  = $value;
    
    }

    public function setName($first_name = 'John', $last_name  = 'Doe')
    {
        
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->name = $this->first_name . ' ' . $this->last_name;
    }
}
