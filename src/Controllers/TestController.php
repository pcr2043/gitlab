<?php


namespace Src\Controllers;


use Src\User;

class TestController
{

    public function __construct()
    {
        $user = new User();

        $data = ['Joe', 'Dao'];
        $method = "setName";
        $user->$method(...$data);
        echo $user->name;
    }
}
